import Vue from 'vue';

export const EventBus = new Vue();

export function error(msg) {
    EventBus.$emit('flash', { msg: msg, status: 'error' });
}

export function notify(msg) {
    EventBus.$emit('flash', { msg: msg, status: 'okay' });
}