const urlBase = '';

export const urlApi = urlBase + '/api/';
export const urlSuggest = urlBase + '/_suggestions/';
export const urlRender = urlBase + '/render/';

export function getPictureUrl(type, _id, picture) {
    return [urlApi, 'files/?_id=', type, '_', _id, '.', picture].join('');
}