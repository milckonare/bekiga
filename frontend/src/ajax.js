import { urlApi } from './urls.js';
import { endpointByType, getRepr } from './helpers.js';
import { notify, error } from './EventBus.js'

const $ = require('jquery');

function postFlaw(flaw) {
    return new Promise(function(resolve, reject) {
        let pictureType = flaw.picture;
        if (!pictureType) {
            pictureType = flaw.pictureFile ? flaw.pictureFile.name.split('.').pop() : '';
        }
        let formData = new FormData();
        formData.append('flaw', flaw.flaw);
        formData.append('notes', flaw.notes);
        formData.append('priority', flaw.priority);
        formData.append('picture', pictureType);

        $.ajax({
            type: 'POST',
            url: urlApi + 'flaws/',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: (resp) => {
                let _id = resp.result._id;
                console.log('FLAW SUBMITTED - ID=' + _id);
                flaw._id = _id;

                if (pictureType) {
                    let pictureForm = new FormData();
                    pictureForm.append('pic', flaw.pictureFile);
                    pictureForm.append('_id', 'flaw_' + _id + '.' + pictureType);
                    $.ajax({
                        url : urlApi + 'files/',
                        data: pictureForm,
                        contentType: false,
                        cache: false,
                        processData: false,
                        type: 'POST',
                        success: (resp) => {
                            console.log('FILE SUBMITTED - ID=' + 'flaw_' + _id + ' FILE=' + flaw.pictureFile);
                        }
                    });
                }

                resolve(_id);
            },
            error: reject
        });
    });
}

function postEntry(entry, index) {
    return new Promise(function(resolve, reject) {
        let deferredFlaws = entry.flawInformation.map(postFlaw);
        $.when(...deferredFlaws).then(() => {
            let flaws = entry.flawInformation.map((i) => i._id);
            let formData = new FormData();
            formData.append('category', entry.category.data._id);
            formData.append('categoryVersion', entry.categoryVersion);
            formData.append('title', entry.title);
            formData.append('manufacturer', entry.manufacturer);
            formData.append('yearBuilt', entry.yearBuilt);
            formData.append('inspectionSigns', entry.inspectionSigns);
            formData.append('manufactureInfoAvailable', entry.manufactureInfoAvailable);
            formData.append('easyAccess', entry.easyAccess);
            formData.append('flaws', flaws);
            formData.append('index', index);

            $.ajax({
                type: 'POST',
                url: urlApi + 'entries/',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: (resp) => {
                    let _id = resp.result._id;
                    console.log('ENTRY SUBMITTED - ID=' + _id);
                    entry._id = _id;

                    resolve(_id);
                },
                error: reject
            });
        }, reject);
    });
}

export function postProtocol(protocol) {
    return new Promise(function(resolve, reject) {
        let deferred = protocol.entries.map((item, index) => postEntry(item.data, index));

        $.when(...deferred).then(() => {
            let entries = protocol.entries.map((i) => i.data._id);
            let formData = new FormData();
            let header = protocol.header;
            formData.append('title', header.title);
            formData.append('inspectionOverview', header.inspectionOverview.data._id);
            formData.append('inspectionDate', header.inspectionDate);
            formData.append('attendees', header.attendees);
            formData.append('facility', header.facility.data._id);
            formData.append('inspector', header.inspector.data._id);
            formData.append('issuer', header.issuer.data._id);
            formData.append('entries', entries);
            $.ajax({
                type: 'POST',
                url: urlApi + 'protocols/',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: (resp) => {
                    let _id = resp.result._id;
                    console.log('PROTOCOL SUBMITTED - ID=' + _id);

                    resolve(_id);
                },
                error: reject
            });
        }, reject);
    });
}

export function fetch(type) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: urlApi + endpointByType(type),
            data: {},
            success: function(resp, status) {
                console.log('FETCHED ' + urlApi + endpointByType(type));
                console.log(JSON.stringify(resp));

                resolve(resp.result.map(function(i) {
                    return {
                        repr: getRepr(i, type),
                        data: i
                    };
                }));
            },
            error: reject
        });
    });
}

function getSimple(_id, type) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url : urlApi + endpointByType(type),
            data: { _id: _id },
            type: 'GET',
            success: (resp) => {
                let res = resp.result;
                resolve({
                    repr: getRepr(res, type),
                    data: res
                });
            },
            error: reject
        });
    });
}

export function getOrganization(_id) {
    return getSimple(_id, 'organization');
}

export function getInspectionStandard(_id) {
    return getSimple(_id, 'inspectionStandard');
}

export function getInspectionOverview(_id) {
    return getSimple(_id, 'inspectionOverview');
}

export function getFacility(_id) {
    return getSimple(_id, 'facility');
}

export function getFlaw(_id) {
    return getSimple(_id, 'flaw');
}

export function getEntry(_id) {
    return new Promise((resolve, reject) => {
        getSimple(_id, 'entry').then((result) => {
            let data = result.data;
            let flaws = data.flaws.split(',').map((i) => {
                let rv = {
                    result: '',
                };
                let promise = new Promise((resolve2, reject2) => {
                    getFlaw(i).then((flaw) => {
                        rv.result = flaw.data;
                        resolve2();
                    }, reject2);
                });

                rv.promise = promise;
                return rv;
            });
            let categoryPromise = new Promise((resolve2, reject2) => {
                getCategory(data.category).then((cat) => {
                    data.category = cat;
                    resolve2();
                }, reject2);
            });
            $.when(...flaws.map((i) => i.promise), categoryPromise).then(() => {
                data.flawInformation = flaws.map((i) => i.result);
                resolve(result);
            }, reject);
        }, reject);
    });
}

export function getCategory(_id) {
    return new Promise((resolve, reject) => {
        getSimple(_id, 'category').then((result) => {
            let data = result.data;
            let standards = data.inspectionStandards.split(',').map((i) => {
                let rv = {
                    result: '',
                };
                let promise = new Promise((resolve2, reject2) => {
                    getInspectionStandard(i).then((std) => {
                        rv.result = std;
                        resolve2();
                    }, reject2);
                });

                rv.promise = promise;
                return rv;
            });

            $.when(...standards.map((i) => i.promise)).then(() => {
                data.inspectionStandards = standards.map((i) => i.result);
                resolve(result);
            }, reject);
        }, reject);
    });
}

export function getPerson(_id) {
    return new Promise((resolve, reject) => {
        getSimple(_id, 'person').then((person) => {
            getSimple(person.data.organization, 'organization').then((org) => {
                person.data.organization = org;
                resolve(person);
            }, reject);
        }, reject);
    });
}

export function getProtocol(_id) {
    return new Promise((resolve, reject) => {
        getSimple(_id, 'protocol').then((result) => {
            let data = result.data;
            let entries = data.entries.split(',').map((i) => {
                let rv = {
                    result: '',
                };
                let promise = new Promise((resolve2, reject2) => {
                    getEntry(i).then((entry) => {
                        rv.result = entry;
                        resolve2();
                    }, reject2);
                });

                rv.promise = promise;
                return rv;
            });

            let deferred = [
                { name: 'facility', get: getFacility },
                { name: 'inspector', get: getPerson },
                { name: 'issuer', get: getOrganization },
                { name: 'inspectionOverview', get: getInspectionOverview }
            ].map((el) => {
                return new Promise((resolve2, reject2) => {
                    el.get(data[el.name]).then((obj) => {
                        data[el.name] = obj;
                        resolve2();
                    }, reject2);
                });
            });

            $.when(...entries.map((i) => i.promise).concat(deferred)).then(() => {
                let protocol = {
                    entries: entries.map((i) => i.result).sort((a, b) => a.data.index.localeCompare(b.data.index)),
                    header: data,
                    _id: _id
                }
                resolve(protocol);
            }, reject);
        }, reject);
    });
}

export function resolveProtocolPreview(protocol) {
    let deferred = [
        { name: 'facility', type: 'facility' },
        { name: 'inspector', type: 'person' },
        { name: 'issuer', type: 'organization' }
    ].map((i) => {
        let promise = new Promise((resolve2, reject2) => {
            getSimple(protocol[i.name], i.type).then((obj) => {
                protocol[i.name] = obj;
                resolve2();
            }, reject2);
        });
        return promise;
    });

    return new Promise((resolve, reject) => {
        $.when(...deferred).then(() => {
            resolve();
        }, reject);
    });
}

export function submit(context, dType, data, requestType, entityName, successHook = null) {
    $.ajax({
        url : urlApi + endpointByType(dType),
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        type: requestType,
        success: (resp) => {
            let _id = context.props._id;
            if (requestType === 'POST') {
                _id = resp.result._id;
            }
            let newItem = {
                data: Object.assign({ _id: _id }, Object(context.props)),
                repr: getRepr(context.props, dType)
            };

            if (successHook) {
                successHook(newItem);
            }

            if (requestType === 'POST') {
                notify(entityName + ' wurde hinzugefügt!');
            } else {
                notify(entityName + ' wurde aktualisiert!');
            }

            context.$parent.$emit('close', { success: true, data: newItem });
        },
        error: () => {
            error('Fehler! ' + entityName + ' konnte nicht gespeichert werden!');
            context.isLoading = false;
        }
    });
}

function deleteSimple(_id, type) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url : urlApi + endpointByType(type),
            data: { _id: _id },
            type: 'DELETE',
            success: (resp) => {
                resolve(resp.result);
            },
            error: reject
        });
    });
}

export function deleteProtocol(_id) {
    return deleteSimple(_id, 'protocol');
}