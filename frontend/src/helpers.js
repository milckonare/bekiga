export function getRepr(obj, type) {
    switch (type) {
    case 'inspectionOverview':
        return obj.text;
    case 'inspectionStandard':
        return 'DIN ' + (obj.hasVersion == 'Ja' ? '(V) ' : '') + obj.din + ' ' + obj.description;
    case 'organization':
    case 'facility':
    case 'category':
        return obj.name;
    case 'person':
        return obj.name + ', ' + obj.firstName;
    case 'protocol':
    case 'entry':
        return obj.title;
    case 'flaw':
        return obj.flaw;
    default:
        return 'ERROR: ' + JSON.stringify(obj) + ' of type "' + type + '"';
    }
}

export function endpointByType(type) {
    switch (type) {
        case 'organization':
            return 'organizations/';
        case 'entry':
            return 'entries/';
        case 'protocol':
            return 'protocols/';
        case 'person':
            return 'persons/';
        case 'category':
            return 'categories/';
        case 'facility':
            return 'facilities/';
        case 'inspectionStandard':
            return 'inspectionStandards/';
        case 'flaw':
            return 'flaws/';
        case 'inspectionOverview':
            return 'inspectionOverview/';
    }
}

function newComplexType() {
    return {
        repr: '',
        data: {
            _id: '',
        }
    }
}

export function newHeaderData() {
    return {
        _id: '',
        title: '',
        inspectionOverview: newComplexType(),
        facility: newComplexType(),
        inspectionDate: '',
        inspector: newComplexType(),
        issuer: newComplexType(),
        attendees: ''
    };

}

export function newEntryData() {
    return {
        category: newComplexType(),
        flawInformation: [],
        title: '',
        manufacturer: '',
        yearBuilt: '',
        inspectionSigns: '',
        manufactureInfoAvailable: 'Keine Angabe',
        easyAccess: 'Keine Angabe',
        _id: ''
    };
}

export function newFlawInfoData() {
    return {
        flaw: '',
        notes: '',
        priority: '',
        picture: '',
        pictureFile: '',
        pictureData: '',
        _id: '',
    };
}
